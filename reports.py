from bokeh.plotting import figure, save, output_file
from bokeh.layouts import column
from bokeh.models import ColumnDataSource, Div
from bokeh.models import (
    HoverTool,
    ZoomInTool,
    ZoomOutTool,
    PanTool,
    SaveTool,
    ResetTool,
)
import pandas
from pandas import DataFrame

output_file(filename="reports.html", title="CX Reports")
# Documents to refer
# https://programminghistorian.org/en/lessons/visualizing-with-bokeh
# https://www.tutorialspoint.com/bokeh/bokeh_columndatasource.htm
# https://www.kaggle.com/code/saurav9786/data-visualization-with-bokeh
# https://docs.bokeh.org/en/latest/docs/user_guide/basic/data.html

header = Div(
    text="<h1>Translation statistics</h1>",
    width=1000,
    height=100,
)
layout: column = column(header)
layout.sizing_mode = "stretch_both"  # set separately to avoid also setting children

# Published section translations
data_source_url: str = "reports/published_cx_translations.tsv"
df: DataFrame = pandas.read_csv(data_source_url, sep="\t")
df["published_date"] = pandas.to_datetime(df["published_date"], format="%Y-%m-%d")

grouped = df.resample(rule="M", on="published_date")["cx2_published_translations"].sum()

grouped = grouped.reset_index()

source: ColumnDataSource = ColumnDataSource(grouped)
cx_total_div = Div(
    text=f"<h2>Total section translations published: {source.data['cx2_published_translations'].sum()}</h2>",
    width=1000,
    height=100,
)
layout.children.append(cx_total_div)

# # create a new plot with a title and axis labels
cx_p1: figure = figure(
    title="Published Translations (Monthly)",
    x_range=[grouped.published_date.min(), grouped.published_date.max()],
    x_axis_label="Date",
    x_axis_type="datetime",
    y_axis_label="cx2_published_translations",
    sizing_mode="stretch_width",
    tools=[
        HoverTool(formatters={'@published_date': 'datetime'}),
        ZoomInTool(),
        ZoomOutTool(),
        PanTool(),
        SaveTool(),
        ResetTool(),
    ],
    tooltips="@cx2_published_translations translations on @published_date{%F}",
)
cx_p1.xaxis.major_label_orientation = 45
cx_p1.line(
    x="published_date", y="cx2_published_translations", source=source, width=1
)
layout.children.append(cx_p1)


# Published section translations
data_source_url: str = "reports/published_section_translations.tsv"
df: DataFrame = pandas.read_csv(data_source_url, sep="\t")


source: ColumnDataSource = ColumnDataSource(df)
sx_total_div = Div(
    text=f"<h2>Total section translations published: {source.data['published_translations'].sum()}</h2>",
    width=1000,
    height=100,
)
layout.children.append(sx_total_div)

# create a new plot with a title and axis labels
sx_p1: figure = figure(
    title="Published Section Translations (Daily)",
    x_range=source.data["published_date"].tolist(),
    x_axis_label="Date",
    y_axis_label="published_translations",
    sizing_mode="stretch_width",
    tools=[
        HoverTool(),
        ZoomInTool(),
        ZoomOutTool(),
        PanTool(),
        SaveTool(),
        ResetTool(),
    ],
    tooltips="@published_translations translations on @published_date",
)
sx_p1.xaxis.major_label_orientation = 45
sx_p1.line(
    x="published_date",
    y="published_translations",
    source=source,
    width=0.9)
layout.children.append(sx_p1)

# Most translated articles

save(layout)
