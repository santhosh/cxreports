SELECT
    translation_source_language as source_language,
    translation_target_language as target_language,
    count(translation_target_language) as no_translations
FROM
    cx_translations
WHERE
    (translation_status = 'published' or translation_target_url is not null)
GROUP BY
    translation_source_language,
    translation_target_language
ORDER BY
    no_translations DESC
;

