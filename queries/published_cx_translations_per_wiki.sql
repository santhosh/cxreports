SELECT
    DATE(translation_start_timestamp) as published_date,
    translation_target_language as language,
    count(translation_target_language) as cx2_published_translations
FROM
    cx_translations
WHERE
    (translation_status = 'published' or translation_target_url is not null)
GROUP BY
    translation_target_language,
    published_date
;
