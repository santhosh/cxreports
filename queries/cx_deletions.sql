SELECT
    DATE_FORMAT(ar_timestamp, "%Y-%m-%d"),
    COUNT(ar_page_id) AS `count`  FROM `change_tag`,`archive`
WHERE
    (ar_rev_id = ct_rev_id)
    AND ct_tag_id = (
        SELECT
            ctd_id
        FROM
            `change_tag_def`
        WHERE
            ctd_name='contenttranslation'
        )
GROUP BY
    YEARWEEK(ar_timestamp, 3);
