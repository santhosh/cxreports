SELECT
    translation_source_language as source_language,
    translation_source_title as source_title,
    count(translation_target_title) as no_translations
FROM
    cx_translations
WHERE
    (translation_status = 'published' or translation_target_url is not null)
GROUP BY
    translation_source_language,
    translation_source_title
ORDER BY
    no_translations DESC
LIMIT
    500
;

