SELECT
    DATE(translation_last_updated_timestamp) as published_date,
    count(cxsx_section_id) as published_translations
FROM
    cx_section_translations, cx_translations
WHERE
    cxsx_translation_status=1 AND
    cxsx_translation_id=translation_id
GROUP BY
    published_date
;
