SELECT
    DATE(translation_start_timestamp) as published_date,
    count(translation_id) as cx2_published_translations
FROM
    cx_translations
WHERE
    (translation_status = 'published' or translation_target_url is not null)
GROUP BY
    published_date
;
