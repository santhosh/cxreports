#!/usr/bin/make -f
SHELL:=/bin/bash
.ONESHELL: # Applies to every targets in the file!
# dumps uses _ instead of - in language codes
PUBLISHED_REPORTS_DIR=/srv/published/datasets/periodic/reports/metrics/cx/

all: published_cx_translations \
	published_section_translations \
	published_section_translations_per_wiki \
	most_translated_articles \
	translation_language_pairs \
	cx_deletions \
	publish

published_cx_translations:
	@echo "Preparing $@"
	analytics-mysql --use-x1 wikishared \
	< "./queries/$@.sql" \
	> "./reports/$@.tsv"

published_section_translations:
	@echo "Preparing $@"
	analytics-mysql --use-x1 wikishared \
	< "./queries/$@.sql" \
	> "./reports/$@.tsv"


published_section_translations_per_wiki:
	@echo "Preparing $@"
	analytics-mysql --use-x1 wikishared \
	< "./queries/$@.sql" \
	> "./reports/$@.tsv"

most_translated_articles:
	@echo "Preparing $@"
	analytics-mysql --use-x1 wikishared \
	< "./queries/$@.sql" \
	> "./reports/$@.tsv"

translation_language_pairs:
	@echo "Preparing $@"
	analytics-mysql --use-x1 wikishared \
	< "./queries/$@.sql" \
	> "./reports/$@.tsv"

cx_deletions: wikis.txt
	rm -rf "reports/$@.tsv"
	sed 's/-/_/g' wikis.txt | while read -r code; do
		echo "Querying deletion stats for $${code}wiki";
		analytics-mysql $${code}wiki \
		< "queries/$@.sql" \
		| sed "s/$$/\t$${code}/" \
		| awk '(NR>1)' \
		>> "reports/$@.tsv"; \
	done
	# Add tsv headers that were removed in above step
	sed -i '1 i\date\tcount\twiki' reports/$@.tsv

publish:
	echo "Publishing reports to $(PUBLISHED_REPORTS_DIR)"
	cp reports/*.tsv $(PUBLISHED_REPORTS_DIR)/
	published-sync

reports:
	python reports.py

clean:
	@rm -rf data dumps

